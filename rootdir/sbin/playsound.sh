#!/system/bin/sh
lines=()
bprop="/system/build.prop"
pattern="^ro\.config\.playsound.$1"
play=0

lines=(${Fruits[@]} $(cat $bprop | egrep -i $pattern))
string="ro.config.playsound.$1=1" 

for i in "${lines[@]}"
do
	if [[ $string = $i ]]; then
		echo "Playing $1 sound"
		play=1;
	fi
	pattern=$(echo "$i" | grep -oc "ro.config.playsound.$1.filename")
	if [[ $pattern ]]; then
		filename=$(echo "$i" | grep -o "[\.a-zA-Z0-9\/]*$")
		echo "Playing $filename"
	fi
done

if [ $play -eq 1 ]; then
	stagefright -a -o $filename
fi

exit 0

