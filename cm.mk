# Release name
PRODUCT_RELEASE_NAME := G4S

# Boot animation
TARGET_SCREEN_HEIGHT := 1280
TARGET_SCREEN_WIDTH := 720

# Inherit device configuration
$(call inherit-product, device/jiayu/g4s/full_g4s.mk)

# Inherit some common CM stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Configure dalvik heap
$(call inherit-product, frameworks/native/build/phone-xhdpi-2048-dalvik-heap.mk)

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := g4s
PRODUCT_NAME := cm_g4s
PRODUCT_BRAND := jiayu
PRODUCT_MODEL := JYT-G4S
PRODUCT_MANUFACTURER := jiayu
